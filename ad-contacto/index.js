const admin = require('firebase-admin');
const express = require('express');
const cors = require('cors');
var permisos = require('./futions/permisos.json');

const app = express();
app.use(express.json());
app.use(express.urlencoded());

app.use(cors({ origin: true }));

app.listen(8002, () => {
    console.log('El servidor Ejecuantando en el puerto 8002')
});

admin.initializeApp({
    credential: admin.credential.cert(permisos),
    databaseURL: 'https://ug-2021c1.firebaseapp.com'
});
const db = admin.firestore();

function salida(codigo, entrada) {
    var today = new Date();
    var date = today.getFullYear() + '-' + today.getMonth() + '-' + today.getDay();
    if (codigo == '200') return {
        mensaje: "Operacion Correcta",
        fecha: date,
        resultado: entrada

    }
    if (codigo == '500') return {
        mensaje: "Ocurrio un error en la api",
        fecha: date,
        resultado: entrada
    }
}

app.get("/api/contacto", async(req, res) => {
    try {
        let colecction = db.collection('ad-contactos');
        const consulta = await colecction.get();
        let docs = consulta.docs;

        const response = docs.map((doc) => ({
            id: doc.id,
            nombre: doc.data().nombre,
            apellido: doc.data().apellido,
            cel: doc.data().cel,
            rol: doc.data().rol,
            email: doc.data().email
        }));


        return res.status(200).json(salida("200", response));
    } catch (error) {
        return res.status(500).json(salida("500", error));
    }

});

app.post("/api/contacto", async(req, res) => {
    try {
        await db.collection('ad-contactos').doc('/' + req.body.id + '/')
            .create({
                nombre: req.body.nombre,
                apellido: req.body.apellido,
                cel: req.body.cel,
                rol: req.body.rol,
                email: req.body.email
            });
        return res.status(200).send();
    } catch (error) {
        console.log(error);
        return res.status(500).json(salida("500", error));
    }

});

app.put("/api/contacto/:contacto_id", async(req, res) => {
    try {
        const documento = db.collection('ad-contactos').doc('/' + req.params.contacto_id + '/')
        await documento.update({
            nombre: req.body.nombre,
            apellido: req.body.apellido,
            cel: req.body.cel,
            rol: req.body.rol,
            email: req.body.email
        });
        return res.status(200).json(salida("200", "Contacto actualziado"));
    } catch (error) {
        console.log(error);
        return res.status(500).json(salida("500", error));
    }

});

app.delete("/api/contacto/:contacto_id", async(req, res) => {
    try {
        const documento = db.collection('ad-contactos').doc('/' + req.params.contacto_id + '/')
        await documento.delete();
        return res.status(200).json(salida("200", "eliminado correctamente"))

    } catch (error) {
        console.log(error);
        return res.status(500).json(salida("500", error));
    }

});